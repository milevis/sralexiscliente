import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {UserService} from './api/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private userService: UserService,
    public navCtrl: NavController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.userService.authenticationState.subscribe(state => {
        console.log('paso', state)
        if (state) {
          this.navCtrl.navigateRoot('/home');
        } else {
          this.navCtrl.navigateRoot('/');
        }
      });
    });
  }

  deslogueo() {
    // cleans out data and sets login page as root:
    this.userService.logout();
    // does navigate user
    this.navCtrl.navigateRoot('login');
  }
}
