import {Component, OnInit, ViewChild } from '@angular/core';
import {UserService} from '../api/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    CurrentUser: any;
    @ViewChild('email') email;
    @ViewChild('password') password;

    constructor(private userService: UserService) {
    }
    ngOnInit() {
        console.log('init');
    }

    atempLogin() {
        console.log(this.email.value,this.password.value)
        this.userService.logout();
        this.userService.login(this.email.value,this.password.value);

    }
}
