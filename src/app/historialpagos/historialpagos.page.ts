import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-historialpagos',
  templateUrl: './historialpagos.page.html',
  styleUrls: ['./historialpagos.page.scss'],
})
export class HistorialpagosPage implements OnInit {
  datos: any;
  constructor() {
    this.datos = [];
  }
  ngOnInit() {
    console.log('init historial')
    if (localStorage.getItem('facturaenvivo')) {
      this.datos = JSON.parse(localStorage.getItem('facturaenvivo'));
      console.log('estos datos',this.datos)
    }
  }

}
