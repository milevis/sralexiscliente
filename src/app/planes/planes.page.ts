import {Component, OnInit} from '@angular/core';
import {PlanService} from "../api/plan.service";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-planes',
    templateUrl: './planes.page.html',
    styleUrls: ['./planes.page.scss'],
})
export class PlanesPage implements OnInit {
    service: any;
    planes: any;

    constructor(private planService: PlanService, private route: ActivatedRoute) {
        this.service = [];
        this.planes = [];
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            /*almaceno el plan seleccionado, en memoria*/
            console.log(params);
            localStorage.setItem('service', JSON.stringify(params));
            this.service = params;
            console.log(this.service);
            this.misplanes();
        });
    }

    misplanes() {
        this.planService.getMisPlanes(this.service.id).subscribe(response => {
            console.log('response', response)
            if (response !== undefined && response.misplanes) {
                this.planes = response.misplanes;
            }
        });
    }
}
