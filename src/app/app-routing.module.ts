import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'plan', loadChildren: './plan/plan.module#PlanPageModule' },
  { path: 'facturacion', loadChildren: './facturacion/facturacion.module#FacturacionPageModule' },
  { path: 'historialpagos', loadChildren: './historialpagos/historialpagos.module#HistorialpagosPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'pagar', loadChildren: './pagar/pagar.module#PagarPageModule' },
  { path: 'pagar/:pseindex',  redirectTo : 'pagar/:pseindex', pathMatch : 'full' },  { path: 'planes', loadChildren: './planes/planes.module#PlanesPageModule' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
