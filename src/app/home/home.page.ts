import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../api/user.service';
import {PlanService} from '../api/plan.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
    dataenlocal: any;
    services: any;
    invoices: any;
    lengthdatos: number;

    constructor(public userService: UserService, private planService: PlanService,  private route: Router ) {
        this.dataenlocal = localStorage;
        this.getServicios();
        this.getInvoices();
    }

    ngOnInit() {
        console.log('init');
    }

    getInvoices(){
        this.planService.facturasPorPlan(false, false).subscribe(response => {
            if ( response !== undefined && response.facturas && response.facturas[0]) {
                this.invoices = response.facturas;
                this.lengthdatos = Object.keys(this.invoices).length;

            }
        });

    }
    gotoPlanes(service){
        console.log(service)
        this.route.navigate(['/planes', { service: service }]);
    }
    getServicios(){
        this.planService.getServicios().subscribe(response => {
            console.log('response', response)
           // if (response !== undefined) {
                this.services = response;
                console.log(this.services);
            //}
        });
    }

}
