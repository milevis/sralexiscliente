import {Component, OnInit} from '@angular/core';
import {PagarService} from '../api/pagar.service';
import {Router} from '@angular/router';
import {ActivatedRoute} from '@angular/router'; // esto es para poder recibir parametros en aqui

import {AlertController} from '@ionic/angular';


@Component({
    selector: 'app-pagar',
    templateUrl: './pagar.page.html',
    styleUrls: ['./pagar.page.scss'],
})

export class PagarPage implements OnInit {

    status: string;

    constructor(private route: ActivatedRoute, private pagarService: PagarService) {
        this.status = this.route.snapshot.paramMap.get('status');
        console.log(this.status);

    }

    ngOnInit() {

    }



}
