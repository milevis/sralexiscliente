import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ActivatedRoute} from '@angular/router';
import {PlanService} from '../api/plan.service';
import {InAppBrowser, InAppBrowserEvent} from '@ionic-native/in-app-browser/ngx';
import * as CryptoJS from 'crypto-js';
import {AlertController} from "@ionic/angular";

@Component({
    selector: 'app-facturacion',
    templateUrl: './facturacion.page.html',
    styleUrls: ['./facturacion.page.scss'],
})
export class FacturacionPage implements OnInit {

    datos: any;
    facturaactual: any;
    dataenlocal: any;
    planactual: any;
    lengthdatos: number;

    constructor(private route: ActivatedRoute, private planService: PlanService,
                private router: Router
        , private iab: InAppBrowser,
                public alertController: AlertController) {
        this.datos = [];
        this.facturaactual = [];
        this.dataenlocal = localStorage;
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            const plan_install_id = params.plan_install_id;
            this.planactual = JSON.parse(localStorage.getItem('planselected'));
            this.planService.facturasPorPlan(plan_install_id, false).subscribe(response => {
                if ( response !== undefined && response.facturas && response.facturas[0]) {
                    this.facturaactual = response.facturas[0];


                    console.log(this.facturaactual);
                    this.datos = response.facturas;
                    this.lengthdatos = Object.keys(this.datos).length;
                    localStorage.setItem('facturaenvivo', JSON.stringify(response.facturas));
                }
            });

        });
    }

    payWithPayu() {

        const amount = this.facturaactual.total;
        const buyerFullName = localStorage.getItem('name');
        const email = localStorage.getItem('user_email');
        const description = this.facturaactual.descripcion_fac;
        const ApiKey = '4Vj8eK4rloUd272L48hsrarnUA';
        const merchantId = '508029';
        const accountId = '512321';
        const currency = 'COP';
        const extra1 = this.facturaactual.id;

        const referenceCode = this.facturaactual.referencia_texto;
        const string = ApiKey + '~' + merchantId + '~' + referenceCode + '~' + amount + '~' + currency;

        const encrypttext = CryptoJS.MD5(string).toString();

        console.log(encrypttext);

        const url = 'https://disegnatec.com/payuform.html?v=17&amount=' + amount + '&buyerFullName=' + buyerFullName
            + '&email=' + email + '&description=' + description + '&signature=' + encrypttext + '&ApiKey=' + ApiKey + '&currency=' + currency
            + '&merchantId=' + merchantId + '&referenceCode=' + referenceCode + '&accountId=' + accountId + '&extra1='
            + extra1;
          const browser = this.iab.create(url, '_blank', 'location=no,toolbar=yes, closebuttoncaption=back, clearcache=yes');
        // browser.insertCSS(...);

      browser.on('loadstop').subscribe((event: InAppBrowserEvent) => {

            console.log(event);
            if (event.url === 'http://cloudmedia.co/crmdesarrollo/login/succesResponsePayu') {
                console.log('pago succes');
                browser.close();
                this.router.navigate(['/pagar', {status: 'success'}]);
            }
            if (event.url === 'http://cloudmedia.co/crmdesarrollo/login/pendingResponsePayu') {
                console.log('pending');
                browser.close();
                this.router.navigate(['/pagar', {status: 'pending'}]);
            }
            if (event.url === 'http://cloudmedia.co/crmdesarrollo/login/errorResponsePayu') {
                console.log('error');
                browser.close();
                this.router.navigate(['/pagar', {status: 'fail'}]);
            }
        });
        browser.on('loadstart').subscribe(event => {
            console.log('start');
            browser.executeScript({
                code: 'console.log(1);'
            });
        });


    }

}
