import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {AlertController} from '@ionic/angular';
import {Platform} from '@ionic/angular';
import {Router} from '@angular/router';
import {throwError, Observable} from 'rxjs';
import {map, retry, catchError, tap} from 'rxjs/operators';

/*cabecera del header post*/
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
    })
};
const base_url = 'http://cloudmedia.co/crmdesarrollo'


const API_KEY_LOGIN = '/x-api-key/'

@Injectable({
    providedIn: 'root'
})
export class PlanService {
    token: any;

    constructor(public http: HttpClient, public alertController: AlertController,
                private platform: Platform, private router: Router) {
        this.platform.ready().then(() => {
            console.log('token aqi', localStorage.getItem('token'))

        });
    }

    /*private getAuthHeaders() {
        return Observable.fromPromise(this.storage.get('token'));
    }*/

    getMisPlanes(servicio_id) {
        try {

            let params = new HttpParams();
            params = params.append('usr_id', localStorage.getItem('usr_id'));
            params = params.append('customer_id', localStorage.getItem('customer_id'));

            params = params.append('serviciomaestro_id ', servicio_id);

            return this.http.post(base_url + '/contactapp/planesporcliente' + API_KEY_LOGIN + localStorage.getItem('token'),
                params, httpOptions)
                .pipe(
                    map((res: any) => {
                        console.log('res', res);
                        if (res.error) {
                            this.presentAlert('Error', '', res.error);
                            return false;
                        }
                        return res;
                    }),
                    catchError((err: HttpErrorResponse) => {
                        let errorMessage = '';
                        if ((err.status == 400) || (err.status == 401) || (err.status == 403)) {
                            errorMessage = `Error Code: ${err.status}.  ${err.error.error}Por favor comunicarse con soporte:`;
                        } else {
                            errorMessage = err.error;
                        }
                        return this.presentAlert('Error', '', errorMessage);
                    })
                );

        } catch (error) {
            console.log('error a ', error);
        }
    }

    getServicios() {
        try {
            let params = new HttpParams();
            params = params.append('usr_id', localStorage.getItem('usr_id'));
            params = params.append('customer_id', localStorage.getItem('customer_id'));

            return this.http.post(base_url + '/servicios/serviciosporcliente' + API_KEY_LOGIN + localStorage.getItem('token'),
                params, httpOptions)
                .pipe(
                    map((res: any) => {
                        console.log('res', res);
                        if (res.error) {
                            this.presentAlert('Error', '', res.error);
                            return false;
                        }
                        return res;
                    }),
                    catchError((err: HttpErrorResponse) => {
                        let errorMessage = '';
                        if ((err.status === 400) || (err.status === 401) || (err.status === 403)) {
                            errorMessage = `Error Code: ${err.status}.  ${err.error.error}Por favor comunicarse con soporte:`;
                        } else {
                            console.log(err);
                            errorMessage = err.error;
                        }
                        return this.presentAlert('Error', '', errorMessage);
                    })
                );

        } catch (error) {
            console.log('error a ', error);
        }
    }

    async presentAlert(cabecera, subtitulo, message) {
        const alert = await this.alertController.create({
            header: cabecera,
            subHeader: subtitulo,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
    }

    facturasPorPlan(plan_install_id, estatus) {

        try {
            let params = new HttpParams();
            params = params.append('usr_id', localStorage.getItem('usr_id'));
            params = params.append('customer_id', localStorage.getItem('customer_id'));
            if(plan_install_id !== false) {
                params = params.append('plan_install_id', plan_install_id);
            } if(estatus !== false) {
                params = params.append('estatus', estatus);
            }
            return this.http.post(base_url + '/invoices/facturas' + API_KEY_LOGIN + localStorage.getItem('token'),
                params, httpOptions)
                .pipe(
                    map((res: any) => {
                        console.log('res', res);
                        if (res.error) {
                            this.presentAlert('Error', '', res.error);
                            return false;
                        }
                        return res;
                    }),
                    catchError((err: HttpErrorResponse) => {
                        let errorMessage = '';
                        if ((err.status == 400) || (err.status == 401) || (err.status == 403)) {
                            errorMessage = `Error Code: ${err.status}.  ${err.error.error}Por favor comunicarse con soporte:`;
                        } else {
                            errorMessage = err.error;
                        }
                        return this.presentAlert('Error', '', errorMessage);
                    })
                );

        } catch (error) {

        }
    }


}


