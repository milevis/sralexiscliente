import { Injectable } from '@angular/core';
import {HttpErrorResponse, HttpParams, HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {AlertController, Platform} from '@ionic/angular';
import {Router} from '@angular/router';

/*cabecera del header post*/
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
  })
};
@Injectable({
  providedIn: 'root'
})

export class PagarService {

  constructor(public http: HttpClient,  public alertController: AlertController,
  private platform: Platform, private router: Router) { }

  getBancosPSE() {

    try {
      const params = {
        'language': 'es',
        'command': 'GET_BANKS_LIST',
        'merchant': {
          'apiLogin': 'pRRXKOl8ikMmt9u',
          'apiKey': '4Vj8eK4rloUd272L48hsrarnUA'
        },
        'test': false,
        'bankListInformation': {
          'paymentMethod': 'PSE',
          'paymentCountry': 'CO'
        }
      }

        return this.http.post('https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi',
            params, httpOptions)
          .pipe(
              map((res: any) => {
                console.log('res', res);
                if (res.error) {
                  this.presentAlert('Error', '', res.error);
                  return false;
                }
                return res;
              }),
              catchError((err: HttpErrorResponse) => {
                let errorMessage = '';
                if ((err.status == 400) || (err.status == 401) || (err.status == 403)) {
                  errorMessage = `Error Code: ${err.status}.  ${err.error.error}Por favor comunicarse con soporte:`;
                } else {
                  errorMessage = err.error;
                }
                return this.presentAlert('Error', '', errorMessage);
              })
          );

    } catch (error) {

    }
  }

  async presentAlert(cabecera, subtitulo, message) {
    const alert = await this.alertController.create({
      header: cabecera,
      subHeader: subtitulo,
      message: message,
      buttons: ['OK']
    });

   //await alert.present();
  }
}
