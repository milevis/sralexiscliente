import {Injectable} from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';
import {map, tap} from 'rxjs/operators';
import {AlertController} from '@ionic/angular';
import {BehaviorSubject} from 'rxjs';
import {Platform} from '@ionic/angular';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    authenticationState = new BehaviorSubject(false);

    constructor(public http: HTTP,  public alertController: AlertController,
                private platform: Platform, private router: Router) {
        this.platform.ready().then(() => {
            this.checkToken();
        });
    }

    checkToken() {
        console.log('primer chequeo', localStorage.getItem('token'));
        if (localStorage.getItem('token')) {
            this.authenticationState.next(true);
        }
    }

    logout() {
        localStorage.removeItem( 'token' );
        localStorage.clear();
        this.authenticationState.next(false);
        return true;

    }

    login(user, passw) {

        this.http.post('http://cloudmedia.co/crmdesarrollo/area/login', {'email': user, 'password': passw}, {})
            .then(data => {
                const datos = JSON.parse(data.data);

                if (datos.success !== undefined) {
                    console.log(datos);
                    this.presentAlert('Felicitaciones', '', 'Logueado con éxito');

                    localStorage.setItem('token', datos.success['api_token']);
                    localStorage.setItem('customer_id', datos.success['customer_id']);
                    localStorage.setItem('usr_id', datos.success['usr_id']);
                    localStorage.setItem('name', datos.success['name']);
                    localStorage.setItem('surname', datos.success['surname']);
                    localStorage.setItem('address', datos.success['address']);
                    localStorage.setItem('nro_identificacion', datos.success['nro_identificacion']);
                    console.log(datos.success);
                    localStorage.setItem('user_email', user);
                    this.authenticationState.next(true);
                    // this.navCtrl.navigateRoot('/home');
                    this.router.navigate(['/home']);
                } else {
                    this.presentAlert('Error', '', datos.error);
                }
                console.log('token despues', localStorage.getItem('token'));
                console.log(data.status);
                console.log(data.data); // data received by server
                console.log(data.headers);
                console.log('datos', datos);
            })
            .catch(error => {

                let errorMessage = '';
                if ((error.status === 400) || (error.status === 401)) {
                    errorMessage = `Error Code: ${error.status}\n Por favor comunicarse con soporte:`;
                } else {
                    errorMessage = error.error;
                }
                return this.presentAlert('Error', '', errorMessage);

            });

    }

    async presentAlert(cabecera, subtitulo, message) {
        const alert = await this.alertController.create({
            header: cabecera,
            subHeader: subtitulo,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
    }
}
