import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router'; // con esto puedo recibir los parametros enviados desde la lista de planes

@Component({
  selector: 'app-plan',
  templateUrl: './plan.page.html',
  styleUrls: ['./plan.page.scss'],
})
export class PlanPage implements OnInit {
  datos: any;
  constructor(private route: ActivatedRoute) {
    this.datos = [];
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      /*almaceno el plan seleccionado, en memoria*/
      localStorage.setItem('planselected',  JSON.stringify(params));
      this.datos = params;
    });
  }

}
