import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ProfilePage } from '../profile/profile';
import { ContactPage } from '../contact/contact';
import { TncPage } from '../tnc/tnc';
import { SigninPage } from '../signin/signin';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {

  constructor(public navCtrl: NavController) {

  }
  
   profile(){
   this.navCtrl.push(ProfilePage);
  }
   contact(){
   this.navCtrl.push(ContactPage);
  }
  tnc(){
   this.navCtrl.push(TncPage);
  }
   signin(){
    this.navCtrl.setRoot(SigninPage);
  }

}
