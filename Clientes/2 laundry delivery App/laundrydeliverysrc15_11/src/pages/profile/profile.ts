import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ChangepasswordPage } from '../changepassword/changepassword';
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  constructor(public navCtrl: NavController) {

  }
  
 changepassword(){
   this.navCtrl.push(ChangepasswordPage);
  }

}
