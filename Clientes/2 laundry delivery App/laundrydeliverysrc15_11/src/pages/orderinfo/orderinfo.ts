import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


@Component({
  selector: 'page-orderinfo',
  templateUrl: 'orderinfo.html'
})
export class OrderinfoPage {

order: string = "order_info";

  constructor(public navCtrl: NavController) {

  }
}
