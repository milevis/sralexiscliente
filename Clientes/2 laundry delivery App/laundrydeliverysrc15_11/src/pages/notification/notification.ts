import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html'
})
export class NotificationPage {

 items = [
    {

      title: "New Delivery Assigned",
      detail: "You have recevived new delivery laundry to be deliver between 01:00 pm to 02:00pm .",
      time: "12:00 pm",
    },
    {
  
      title: "New Pick up Assigned",
      detail: "You have recevived new delivery laundry to be deliver between 01:00 pm to 02:00pm .",
      time: "12:00 pm",
    },
    {
     
      title: "New Delivery Assigned",
      detail: "You have recevived new delivery laundry to be deliver between 01:00 pm to 02:00pm .",
      time: "12:00 pm",
    },
    {
     
      title: "New Pick up Assigned",
      detail: "You have recevived new delivery laundry to be deliver between 01:00 pm to 02:00pm .",
      time: "12:00 pm",
    }
    ,
    {
  
      title: "New Delivery Assigned",
      detail: "You have recevived new delivery laundry to be deliver between 01:00 pm to 02:00pm .",
      time: "12:00 pm",
    },
    {
     
      title: "New Pick up Assigned",
      detail: "You have recevived new delivery laundry to be deliver between 01:00 pm to 02:00pm .",
      time: "12:00 pm",
    },
    {
    
      title: "New Delivery Assigned",
      detail: "You have recevived new delivery laundry to be deliver between 01:00 pm to 02:00pm .",
      time: "12:00 pm",
    }
  ];

  constructor(public navCtrl: NavController) {

  }

}
