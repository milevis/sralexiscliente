import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { OrderinfoPage } from '../orderinfo/orderinfo';

@Component({
  selector: 'page-pickup',
  templateUrl: 'pickup.html'
})
export class PickupPage {

status: string = "assigned";

  constructor(public navCtrl: NavController) {

  }
  orderinfo(){
        this.navCtrl.push(OrderinfoPage)
  }
}
