import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-earnings',
  templateUrl: 'earnings.html'
})
export class EarningsPage {
 earnings: string = "m1";
 times: string = "d1";
  constructor(public navCtrl: NavController) {

  }

}
