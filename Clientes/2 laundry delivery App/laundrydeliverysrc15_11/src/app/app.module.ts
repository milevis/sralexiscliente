import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AccountPage } from '../pages/account/account';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { ContactPage } from '../pages/contact/contact';
import { EarningsPage } from '../pages/earnings/earnings';
import { NotificationPage } from '../pages/notification/notification';
import { OtpPage } from '../pages/otp/otp';
import { OrderinfoPage } from '../pages/orderinfo/orderinfo';
import { PasswordPage } from '../pages/password/password';
import { PickupPage } from '../pages/pickup/pickup';
import { ProfilePage } from '../pages/profile/profile';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs/tabs';
import { TncPage } from '../pages/tnc/tnc';
 
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    AccountPage,
    ChangepasswordPage,
    ContactPage,
    EarningsPage,
    NotificationPage,
    OtpPage,
    OrderinfoPage,
    PasswordPage,
    PickupPage,
    ProfilePage,
    SigninPage,
    SignupPage,
    TabsPage,
    TncPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AccountPage,
    ChangepasswordPage,
    ContactPage,
    EarningsPage,
    NotificationPage,
    OtpPage,
    OrderinfoPage,
    PasswordPage,
    PickupPage,
    ProfilePage,
    SigninPage,
    SignupPage,
    TabsPage,
    TncPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
