import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { AccountPage } from '../pages/account/account';
import { AddaddressPage } from '../pages/addaddress/addaddress';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { MyordersPage } from '../pages/myorders/myorders';
import { NotificationPage } from '../pages/notification/notification';
import { OffersPage } from '../pages/offers/offers';
import { OrderconfirmedPage } from '../pages/orderconfirmed/orderconfirmed';
import { OrderslipPage } from '../pages/orderslip/orderslip';
import { OtpPage } from '../pages/otp/otp';
import { PasswordPage } from '../pages/password/password';
import { PaymentPage } from '../pages/payment/payment';
import { ProfilePage } from '../pages/profile/profile';
import { RatePage } from '../pages/rate/rate';
import { SelectaddressPage } from '../pages/selectaddress/selectaddress';
import { SelectclothesPage } from '../pages/selectclothes/selectclothes';
import { SelectdatePage } from '../pages/selectdate/selectdate';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs/tabs';
import { TncPage } from '../pages/tnc/tnc';
 
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    AccountPage,
    AddaddressPage,
    ContactPage,
    HomePage,
    MyordersPage,
    NotificationPage,
    OffersPage,
    OrderconfirmedPage,
    OrderslipPage,
    OtpPage,
    PasswordPage,
    PaymentPage,
    ProfilePage,
    RatePage,
    SelectaddressPage,
    SelectclothesPage,
    SelectdatePage,
    SigninPage,
    SignupPage,
    TabsPage,
    TncPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    AccountPage,
    AddaddressPage,
    ContactPage,
    HomePage,
    MyordersPage,
    NotificationPage,
    OffersPage,
    OrderconfirmedPage,
    OrderslipPage,
    OtpPage,
    PasswordPage,
    PaymentPage,
    ProfilePage,
    RatePage,
    SelectaddressPage,
    SelectclothesPage,
    SelectdatePage,
    SigninPage,
    SignupPage,
    TabsPage,
    TncPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
