import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ProfilePage } from '../profile/profile';
import { MyordersPage } from '../myorders/myorders';
import { AddaddressPage } from '../addaddress/addaddress';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { TncPage } from '../tnc/tnc';
import { SigninPage } from '../signin/signin';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {

  constructor(public navCtrl: NavController) {

  }
  
   profile(){
   this.navCtrl.push(ProfilePage);
  }
   myorders(){
   this.navCtrl.push(MyordersPage);
  }
   addaddress(){
   this.navCtrl.push(AddaddressPage);
  }
   about(){
   this.navCtrl.push(AboutPage);
  }
   contact(){
   this.navCtrl.push(ContactPage);
  }
  tnc(){
   this.navCtrl.push(TncPage);
  }
   signin(){
    this.navCtrl.setRoot(SigninPage);
  }

}
