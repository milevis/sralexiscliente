import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SelectclothesPage } from '../selectclothes/selectclothes';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  slides = [
    {
      image: "assets/imgs/banner.png",
      title: "Flat 50% off on <br>First Order",
    },
    {
      image: "assets/imgs/banner.png",
      title: "Flat 50% off on <br>First Order",
    },
    {
      image: "assets/imgs/banner.png",
      title: "Flat 50% off on <br>First Order",
    }
  ];

  services = [
    {
      image: "assets/imgs/washing.png",
      title: "Wash & Fold",
      small: "Min 12 Hours",
    },
    {
      image: "assets/imgs/iron.png",
      title: "Wash & Iorn",
      small: "Min 6 Hours",
    },
    {
      image: "assets/imgs/dryclean.png",
      title: "Dry Clean",
      small: "Min 24 Hours",
    }
  ];

  constructor(public navCtrl: NavController) {

  }
  
  selectclothes(){
        this.navCtrl.push(SelectclothesPage)
  }
}
